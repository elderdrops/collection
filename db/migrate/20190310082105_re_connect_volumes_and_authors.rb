class ReConnectVolumesAndAuthors < ActiveRecord::Migration[5.2]
  def change
    drop_table :volumes_authors

    create_table :authors_volumes, id: false do |t|
      t.belongs_to :volume, index: true
      t.belongs_to :author, index: true
    end
  end
end
