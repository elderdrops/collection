class CreateUserVolumeConnection < ActiveRecord::Migration[5.2]
  def change
    create_table :users_volumes do |t|
      t.belongs_to :volume, index: true
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
