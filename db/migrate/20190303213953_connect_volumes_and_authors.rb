class ConnectVolumesAndAuthors < ActiveRecord::Migration[5.2]
  def change
    create_table :volumes_authors, id: false do |t|
      t.belongs_to :volume, index: true
      t.belongs_to :author, index: true
    end
  end
end
