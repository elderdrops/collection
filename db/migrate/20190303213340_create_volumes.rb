class CreateVolumes < ActiveRecord::Migration[5.2]
  def change
    create_table :volumes do |t|
      t.belongs_to :series, index: true
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
