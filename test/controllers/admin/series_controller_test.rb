require 'test_helper'

class Admin::SeriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @seire = seires(:one)
  end

  test "should get index" do
    get admin_seires_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_seire_url
    assert_response :success
  end

  test "should create seire" do
    assert_difference('Seire.count') do
      post admin_seires_url, params: { seire: {  } }
    end

    assert_redirected_to seire_url(Seire.last)
  end

  test "should show seire" do
    get admin_seire_url(@seire)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_seire_url(@seire)
    assert_response :success
  end

  test "should update seire" do
    patch admin_seire_url(@seire), params: { seire: {  } }
    assert_redirected_to seire_url(@seire)
  end

  test "should destroy seire" do
    assert_difference('Seire.count', -1) do
      delete admin_seire_url(@seire)
    end

    assert_redirected_to admin_seires_url
  end
end
