require 'test_helper'

class Admin::VolumesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @volume = volumes(:one)
  end

  test "should get index" do
    get admin_volumes_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_volume_url
    assert_response :success
  end

  test "should create volume" do
    assert_difference('Volume.count') do
      post admin_volumes_url, params: { volume: {  } }
    end

    assert_redirected_to volume_url(Volume.last)
  end

  test "should show volume" do
    get admin_volume_url(@volume)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_volume_url(@volume)
    assert_response :success
  end

  test "should update volume" do
    patch admin_volume_url(@volume), params: { volume: {  } }
    assert_redirected_to volume_url(@volume)
  end

  test "should destroy volume" do
    assert_difference('Volume.count', -1) do
      delete admin_volume_url(@volume)
    end

    assert_redirected_to admin_volumes_url
  end
end
