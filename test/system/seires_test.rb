require "application_system_test_case"

class SeiresTest < ApplicationSystemTestCase
  setup do
    @seire = seires(:one)
  end

  test "visiting the index" do
    visit seires_url
    assert_selector "h1", text: "Seires"
  end

  test "creating a Seire" do
    visit seires_url
    click_on "New Seire"

    click_on "Create Seire"

    assert_text "Seire was successfully created"
    click_on "Back"
  end

  test "updating a Seire" do
    visit seires_url
    click_on "Edit", match: :first

    click_on "Update Seire"

    assert_text "Seire was successfully updated"
    click_on "Back"
  end

  test "destroying a Seire" do
    visit seires_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Seire was successfully destroyed"
  end
end
