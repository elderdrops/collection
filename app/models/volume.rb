class Volume < ApplicationRecord
  belongs_to :series
  has_and_belongs_to_many :authors
  has_and_belongs_to_many :users
end
