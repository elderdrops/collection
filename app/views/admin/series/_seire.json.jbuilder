json.extract! seire, :id, :created_at, :updated_at
json.url seire_url(seire, format: :json)
