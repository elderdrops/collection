module VolumeService
  class VolumeToAuthorAssign
    
    def initialize(ids, volume)
      @author_ids = ids
      @volume = volume
    end

    def assign
      @author_ids.each do |id|
        unless id.empty?
          @volume.authors << Author.find(id)
        end
      end
    end

  end
end
