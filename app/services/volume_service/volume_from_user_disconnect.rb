module VolumeService
  class VolumeFromUserDisconnect
    def initialize(user, volume)
      @user = user
      @volume = volume
    end

    def exec
      if @volume.users.include? @user
        @volume.users.delete @user
      end
    end

  end
end