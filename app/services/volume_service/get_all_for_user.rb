module VolumeService
  class GetAllForUser

    def initialize user_id
      @user_id = user_id
    end

    def exec
      user = User.find(@user_id)
      user.volumes
    end


  end
end