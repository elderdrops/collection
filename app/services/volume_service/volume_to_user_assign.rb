module VolumeService
  class VolumeToUserAssign
    def initialize(user, volume)
      @user = user
      @volume = volume
    end

    def exec
      unless @volume.users.include? @user
          @volume.users << @user
      end
    end
  end
end