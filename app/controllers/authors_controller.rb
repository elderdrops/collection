class AuthorsController < ApplicationController
#  before_action :set_author, only: [:show, :edit, :update, :destroy]
#  before_action :authenticate_user!, only: [:edit, :update, :create, :new]

  # GET /authors
  # GET /authors.json
  def index
    @authors = Author.all
  end

  # GET /authors/1
  # GET /authors/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_author
      @author = Author.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def author_params
      params.require(:author).permit(:first_name, :last_name, :description)
    end
end
