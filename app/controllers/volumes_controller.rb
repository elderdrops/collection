class VolumesController < ApplicationController
  before_action :set_volume, only: [:show, :edit, :update, :destroy, :assign_to_user, :disconnect_from_user]


  # GET /volumes
  # GET /volumes.json
  def index
    @volumes = Volume.all
  end

  # GET /volumes/1
  # GET /volumes/1.json
  def show
  end

  def assign_to_user
    VolumeService::VolumeToUserAssign.new(current_user, @volume).exec
    respond_to do |format|
      if @volume.save
        format.html { redirect_to @volume, notice: 'Volume was successfully assign.' }
        format.json { render :show, status: :created, location: @volume }
      else
        format.html { render :new }
        format.json { render json: @volume.errors, status: :unprocessable_entity }
      end
    end
  end

  def disconnect_from_user
    VolumeService::VolumeFromUserDisconnect.new(current_user, @volume).exec
    respond_to do |format|
      if @volume.save
        format.html { redirect_to @volume, notice: 'Volume was successfully disconnected.' }
        format.json { render :show, status: :created, location: @volume }
      else
        format.html { render :new }
        format.json { render json: @volume.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_volume
      @volume = Volume.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def volume_params
      params.require(:volume).permit(:title, :description, {author_ids: [:id]}, :series_id)
    end

    def set_form_relations
      @series = Series.all
      @authors = Author.all
    end
end
