class Admin::SeriesController < ApplicationController
  before_action :set_seire, only: [:show, :edit, :update, :destroy]

  # GET /seires
  # GET /seires.json
  def index
    @seires = Seire.all
  end

  # GET /seires/1
  # GET /seires/1.json
  def show
  end

  # GET /seires/new
  def new
    @seire = Seire.new
  end

  # GET /seires/1/edit
  def edit
  end

  # POST /seires
  # POST /seires.json
  def create
    @seire = Seire.new(seire_params)

    respond_to do |format|
      if @seire.save
        format.html { redirect_to @seire, notice: 'Seire was successfully created.' }
        format.json { render :show, status: :created, location: @seire }
      else
        format.html { render :new }
        format.json { render json: @seire.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seires/1
  # PATCH/PUT /seires/1.json
  def update
    respond_to do |format|
      if @seire.update(seire_params)
        format.html { redirect_to @seire, notice: 'Seire was successfully updated.' }
        format.json { render :show, status: :ok, location: @seire }
      else
        format.html { render :edit }
        format.json { render json: @seire.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seires/1
  # DELETE /seires/1.json
  def destroy
    @seire.destroy
    respond_to do |format|
      format.html { redirect_to admin_seires_url, notice: 'Seire was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seire
      @seire = Seire.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seire_params
      params.fetch(:seire, {})
    end
end
