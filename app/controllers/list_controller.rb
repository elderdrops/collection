class ListController < ApplicationController

  def list
    @volumes = VolumeService::GetAllForUser.new(user_params).exec
  end

private

  def user_params
    params[:id]
  end
end
