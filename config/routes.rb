Rails.application.routes.draw do

  devise_for :users
  get 'landing/index'
  root :to => 'landing#index'

  match 'volume/:id', to: 'volumes#assign_to_user',  via: [:post], as: 'assign_volume_to_user'
  match 'volume/:id', to: 'volumes#disconnect_from_user',  via: [:delete], as: 'disconnect_volume_from_user'

  get 'list/:id', to: 'list#list',  via: [:get], as: 'list'

  resources :series, :authors, :volumes
  namespace :admin do
    resources :authors, :volumes, :series
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
