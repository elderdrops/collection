FROM ruby:latest

RUN apt-get update && \
    apt-get install -y vim \
                       mysql-client \
                       --no-install-recommends

RUN apt-get install -y git-core curl build-essential openssl libssl-dev nodejs

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
apt-get install nodejs
# yarnパッケージ管理ツール
RUN apt-get update && apt-get install -y curl apt-transport-https wget && \
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
apt-get update && apt-get install -y yarn


# Rails depends
RUN gem install rails -v '5.2.2'
RUN gem install bundle

WORKDIR /app
ADD Gemfile Gemfile.lock /app/
RUN bundle install

ADD . .
CMD ["puma"]